#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/msg.h>

struct text_message{
    long mtype;
    char mtext[100];
};

void sendMessage(struct text_message *mess, int msid);
void receiveMessage(struct text_message *mess, int msid);

int main(int argc, char **argv)
{
    int msid, v;
    int state = 0;
    struct text_message mess;
    mess.mtype = 1;
    msid = msgget(1234, IPC_CREAT | 0666);

    if(msid == -1)
    {
        exit(1);
    }

    for(;;)
    {
        switch(state)
        {
            case 0:
            {
                sendMessage(&mess, msid);
                state = 1;
                break;
            }
            case 1:
            {
                receiveMessage(&mess, msid);
                state = 0;
                break;
            }
        }
    }
}

void sendMessage(struct text_message *mess, int msid)
{
    int v;
    scanf("%s", mess->mtext);
     v = msgsnd(msid, mess, strlen(mess->mtext) + 1, 0);

    if(v < 0)
    {
       printf("Error sending message!\n");
       exit(1);
    }
    else
    {
        if(!strcmp("exit", mess->mtext))
        {
            printf("Exiting\n");
            exit(0);
        }
    }
}

void receiveMessage(struct text_message *mess, int msid)
{
    int v;
    v = msgrcv(msid, mess, 100, 1, 0);

    if(v < 0)
    {
        printf("Error getting message\n");
        exit(2);
    }
    else
    {
        printf("%s\n", mess->mtext);
        if(!strcmp("exit", mess->mtext))
        {
            msgctl(msid, IPC_RMID, 0);
            exit(0);
        }
    }

}
