/*
 * lab03.c
 *
 * author: Chandler Ellis
 */
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <string.h>
#include <sys/wait.h>
#include <fcntl.h>

#define BUFFER_LEN 1024

int main(int argc, char **argv)
{
    char line_buffer[BUFFER_LEN];
    char *command [100];


    for(;;)
    {
        uint8_t pipeFound = 0;
        int pipeIndex = 0;
        uint8_t redirectFound = 0;
        int redirectIndex = 0;

        printf("shell>> ");

        //get line from std input
        if(!fgets(line_buffer, BUFFER_LEN, stdin))
        {
            break;
        }

        //remove newline character, replace with string terminator
        if(line_buffer[strlen(line_buffer) - 1] == '\n')
        {
            line_buffer[strlen(line_buffer) - 1] = '\0';
        }

        //tokenize command
        char *token;
        token = strtok(line_buffer, " ");
        int counter = 0;
        while(token != NULL && counter < 100)
        {
            if(strcmp(token, ">") == 0)
            {
                redirectFound = 1;
                redirectIndex = counter;
            }
            else if(strcmp(token, "|") == 0)
            {
                pipeFound = 1;
                pipeIndex = counter;
            }
            command[counter] = token;
            token = strtok(NULL, " ");
            counter++;
        }

        command[counter] = NULL;

        if(!strcmp(command[0], "exit"))
        {
            break;
        }

        if(!pipeFound && !redirectFound)
        {
            int id;
            if((id = fork()) == 0)
            {
                if(execvp(command[0], command)  < 0)
                {
                    printf("Failed to execute command\n");
                }
            }
            else if(id < 0)
            {
                printf("Failed to make child.. \n");
            }
            else
            {
                wait(0);
            }
        }
        else if(!pipeFound && redirectFound)
        {
            char *array1[10];
            for(int i = 0; i < redirectIndex; i++)
            {
                array1[i] = command[i];
            }

            array1[redirectIndex] = NULL;

            int descriptor = open(command[redirectIndex + 1], O_CREAT | O_RDWR | O_TRUNC, 0700);
            int id;
            if((id = fork()) == 0)
            {
                dup2(descriptor, 1);
                execvp(array1[0], array1);
                close(descriptor);
                exit(0);
            }
            else if(id != 0)
            {
                wait(0);
            }
        }
        else if(pipeFound && !redirectFound)
        {
            char *array1[20];
            char *array2[20];
            for(int i = 0; i < pipeIndex; i++)
            {
                array1[i] = command[i];
            }
            array1[pipeIndex] = NULL;

            int j = 0;
            for(int i = (pipeIndex + 1); i < 10; i++)
            {
                array2[j] = command[i];
                if(command[j] == NULL)
                {
                    break;
                }
                j++;
            }

            array2[j] = NULL;
            int pipeDes[2];

            if(pipe(pipeDes) == -1)
            {
                printf("Error in calling the piping function\n");
                exit(0);
            }

            if(fork() == 0)
            {
                dup2(pipeDes[1], 1);
                close(pipeDes[0]);
                execvp(array1[0], array1);
                exit(0);
            }
            else if(fork() == 0)
            {
                close(pipeDes[1]);
                dup2(pipeDes[0], 0);
                execvp(array2[0], array2);
                exit(0);
            }
            else
            {
                close(pipeDes[0]);
                close(pipeDes[1]);
                wait(0);
                wait(0);
            }
        }
        else if(pipeFound && redirectFound)
        {
            char *array1[10];
            char *array2[10];

            for(int i = 0; i < pipeIndex; i++)
            {
                array1[i] = command[i];
            }
            array1[pipeIndex] = NULL;

            int j = 0;
            for(int i = (pipeIndex + 1); i < redirectIndex; i++)
            {
                array2[j] = command[i];
                if(command[i] == NULL)
                {
                    break;
                }
                j++;
            }
            array2[j] = NULL;
            
        
            int pipeDes[2];     
            int stdout_cp = dup(1);
            close(1);
            int descriptor = open(command[redirectIndex + 1], O_CREAT | O_WRONLY | O_TRUNC, 0664);            

            if(pipe(pipeDes) == -1)
            {
                fprintf(stderr, "Error in calling the piping function\n");
                dup2(stdout_cp, 1);
                exit(0);
            }

            
            int id = fork();
            if(id == 0)
            {                
                close(pipeDes[0]);
                if(dup2(pipeDes[1], descriptor) == -1)                
                {
                    exit(EXIT_FAILURE);
                }
                
                if(execvp(array1[0], array1) == -1)
                {
                    exit(EXIT_FAILURE);
                }
                close(pipeDes[1]);  
                exit(0);
            }
            else if(id > 0)
            {       
                wait(NULL);
                close(pipeDes[1]);
                if(dup2(pipeDes[0], STDIN_FILENO) == -1)
                {
                    exit(EXIT_FAILURE);
                }
                
                if(execvp(array2[0], array2) == -1)
                {
                    exit(EXIT_FAILURE);
                }
                close(pipeDes[0]);  
                
            }
            else
            {                
                exit(EXIT_FAILURE);
                fprintf(stderr, "fork failed\n");                
            }
                      
            dup2(stdout_cp, 1);
        }

    }
    return 0;
}
