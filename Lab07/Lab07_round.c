/*

*/

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "process.h"

void get_process_information(int num_processes, struct process *p);

void schedule(int num_processes, int quantum, struct process *p);

int main(int argc, char **argv)
{
    int cpu_quantum = 0;
    int num_processes = 0;
    int count = 0;

    printf("Enter number of processes: ");
    scanf("%i", &num_processes);
    printf("Enter quantum time unit: ");
    scanf("%i", &cpu_quantum);

    struct process *p = malloc(num_processes * sizeof(struct process)); //dynamically create process array

    get_process_information(num_processes, p);

    schedule(num_processes, cpu_quantum, p);

    free(p);
}

void get_process_information(int num_processes, struct process *p)
{
    for(int i = 0; i < num_processes; i++)
    {
        printf("Enter id of process: %i\n", i);
        scanf("%i", &(p[i].pid));
        printf("Enter burst time in for process: %i\n", i);
        scanf("%i", &(p[i].burst_time));

        p[i].working_time = 0;
        p[i].t_round = 0;
    }
}

void schedule(int num_processes, int quantum, struct process *p)
{
    int t = 0;
    for(;;)
    {
        int done = 1;

        for(int i = 0; i < num_processes; i++)
        {
            if(p[i].working_time < p[i].burst_time)
            {
                done = 0;

                int rem_burst_time = p[i].burst_time - p[i].working_time;
                if(rem_burst_time > quantum)
                {
                    p[i].working_time += quantum;
                    t += quantum;
                }
                else
                {
                    t = t + rem_burst_time;
                    p[i].working_time = p[i].working_time + p[i].burst_time;

                }
            }
        }
        if(done)
        {
            break;
        }
    }

    int average_wait = 0;
    int average_t_round = 0;

    printf("Total time: %i\n", t);
    for(int i = 0; i < num_processes; i++)
    {
        p[i].t_round = (t - p[i].working_time) + p[i].burst_time;
        average_wait += (t - p[i].working_time);
        average_t_round += p[i].t_round;
        printf("Process: %i, Burst Time: %i, Wait Time: %i, Turnaround Time: %i\n", p[i].pid,p[i].burst_time, t - p[i].working_time, p[i].t_round);
    }
    printf("Average wait: %i\n", average_wait / num_processes);
    printf("Average Turnaround: %i\n", average_t_round / num_processes);
}

