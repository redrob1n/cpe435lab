/*

*/

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "process.h"

void get_process_information(int num_processes, struct process_priority *p);

void schedule(int num_processes, int quantum, struct process_priority *p);

int compare(const void *a, const void *b);

int main(int argc, char **argv)
{
    int cpu_quantum = 0;
    int num_processes = 0;
    int count = 0;

    printf("Enter number of processes: ");
    scanf("%i", &num_processes);

    struct process_priority *p = malloc(num_processes * sizeof(struct process_priority)); //dynamically create process array

    get_process_information(num_processes, p);

    schedule(num_processes, cpu_quantum, p);

    free(p);
}

void get_process_information(int num_processes, struct process_priority *p)
{
    for(int i = 0; i < num_processes; i++)
    {
        printf("Enter id of process: %i\n", i);
        scanf("%i", &(p[i].pid));
        printf("Enter burst time in for process: %i\n", i);
        scanf("%i", &(p[i].burst_time));
        printf("Enter priority for process: %i\n", i);
        scanf("%i", &(p[i].priority));

        p[i].working_time = 0;
        p[i].t_round = 0;
    }
}

void schedule(int num_processes, int quantum, struct process_priority *p)
{
    int t = 0;
    int total_burst = 0;
    int average_wait = 0;
    int average_t_round = 0;
    qsort((void*)p, num_processes, sizeof(p[0]), compare);

    for(int i = 0; i < num_processes; i++)
    {
        total_burst += p[i].burst_time;
    }
    int wait_times[num_processes];
    while(t < total_burst)
    {
        for(int i = 0; i < num_processes; i++)
        {
            p[i].t_round += p[i].burst_time + t;
            t += p[i].burst_time;
            wait_times[i] = p[i].t_round - p[i].burst_time;
            average_wait += p[i].t_round - p[i].burst_time;
            average_t_round += p[i].t_round;
        }
    }

    for(int i = 0; i < num_processes; i++)
    {
        printf("Process: %i, Burst Time %i, Wait Time: %i, Turnaround Time: %i\n", p[i].pid, p[i].burst_time, wait_times[i], p[i].t_round);
    }
    printf("Average wait: %i\n", average_wait / num_processes);
    printf("Average Turnaround: %i\n", average_t_round / num_processes);
}

int compare(const void *a, const void *b)
{
    return ((*(struct process_priority *)a).priority - (*(struct process_priority *)b).priority);
}

