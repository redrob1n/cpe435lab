

#ifndef _PROCESS_H_
#define _PROCESS_H_

struct process {
    int pid; //process id
    int burst_time; //CPU burst time
    int working_time; //time this process executed
    int t_round; //turn around time
};

struct process_priority {
    int pid; //process id
    int priority;
    int burst_time; //CPU burst time
    int working_time; //time this process executed
    int t_round; //turn around time
};

#endif /* _PROCESS_H_ */
