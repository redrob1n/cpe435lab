/*
 * Lab02/exercise3.c
 *
 * author: Chandler Ellis
 */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>

int main(int argc, char **argv)
{
    //handle not enough arguments
    if(argc != 2)
    {
        printf("Error: enter ONE even integer as an argument\n");
        exit(1);
    }

    //handle non-even arguments
    int numChild = atoi(argv[1]);
    if(numChild % 2 != 0)
    {
        printf("Error: argument must be an even number!\n");
        exit(2);
    }

    //create n child processes, output their pid and their order
    for(int i = 0; i < numChild; i++)
    {
        if(fork() == 0)
        {
            printf("I am child %i and my pid is %i\n", i, getpid());
            exit(0);
        }
    }

    return 0;
}
