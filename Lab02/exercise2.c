/*
 * Lab02/exercise2.c
 *
 * author: Chandler Ellis
 */

#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>

int main(int argc, char *argv[])
{
    //handle incorrect number of arguments
    if(argc != 3)
    {
        printf("Error: enter two integers as arguments, you entered: %i\n", argc);
        exit(1);
    }

    int val1 = atoi(argv[1]);
    int val2 = atoi(argv[2]);

    int result = 0;

    int pid = fork(); //create first child process

    if(pid == 0)
    {
        result = subtract(val2, val1); //perform subtraction
        printf("This is child1, my pid is: %i and the value of result after subtraction is: %i\n",getpid(), result);

        if(fork() == 0) //create second child process
        {
            result = val1 + val2; //perform addition
            printf("This is child2, my pid is %i and the value of result after addition is: %i\n", getpid(), result);
        }
        else
        {
            wait(NULL);
            result = val1 * val2; //perform multiplication
            printf("This is child1, result of multiply is: %i\n", result);
        }
    }
    else
    {
        printf("I am the parent. My pid is: %i. I will wait for for termination of child1\n", getpid());
        int cpid = wait(NULL); //wait for child1 to terminate
        printf("I am the parent. Process completed pid of child that terminated is: %i\n", cpid);
    }

    return 0;
}

int subtract(int arg1, int arg2)
{
    return arg1 - arg2;
}

