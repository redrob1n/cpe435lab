#include <stdio.h>

int main()
{
    int val = 0;
    int pid = fork();
    if(pid == 0)
    {
        val += 2;
        printf("I am the child 'val' is: %i and my pid is: %i\n", val, getpid());
    }
    else
    {
        val += 5;
        printf("I am the parent. 'val' is: %i and my pid is: %i\n", val, getpid());
    }

    return 0;
}
