/*
 * File: lab04.c
 *
 * author: Chandler Ellis
 */

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <math.h>
#include <sys/time.h>

#define MAX_THREADS 10
const double b = 1.0;
const double a = 0.0;

int num_rect = 0;

void print_usage(void);

void *integral(void *thread_area);

int main(int argc, char **argv)
{
    if(argc != 2)
    {
        print_usage();
        exit(1);
    }

    num_rect = atoi(argv[1]);

    if(num_rect < MAX_THREADS)
    {
        printf("Error: argument must be greater than or equal to the maximum number of threads(%i)\n", MAX_THREADS);
        print_usage();
        exit(2);
    }

    double area = 0;

    /* Serial portion of program */
    struct timeval start_serial, end_serial;
    gettimeofday(&start_serial, NULL);

    double step = (b - a) / num_rect;
    for(double i = 0; i < b; i = (i + step))
    {
        area += step * sqrt(1-i*i);
    }
    area *= 4;

    gettimeofday(&end_serial, NULL);
    float total_serial_time = (end_serial.tv_sec * 1000000 + end_serial.tv_usec) - (start_serial.tv_sec * 1000000 + start_serial.tv_usec);

    printf("The result of the area using the serial method is: %f\n", area);
    printf("Execution time for the serial method is %f ns\n", total_serial_time);

    /* End Serial portion */

    /* Parallel portion of program */
    struct timeval start_parallel, end_parallel;
    gettimeofday(&start_parallel, NULL);

    pthread_t *threads = malloc(MAX_THREADS * sizeof(*threads));
    double * thread_area = malloc(MAX_THREADS * sizeof(*thread_area));

    int rc;
    for(int t = 0; t < MAX_THREADS; t++)
    {
        rc = pthread_create(&threads[t], NULL, integral, (void*)(thread_area + t));
        thread_area[t] = t;
        if(rc)
        {
            printf("Error creating new thread\n");
            exit(3);
        }
    }

    area = 0;
    int rs;
    for(int i = 0; i < MAX_THREADS;i++)
    {
        rs = pthread_join(threads[i], NULL);
        area = area + thread_area[i];
        if(rs)
        {
            printf("Error creating new thread\n");
            exit(3);
        }
    }

    area = area * 4;
    gettimeofday(&end_parallel, NULL);
    float total_parallel_time = (end_parallel.tv_sec - start_parallel.tv_sec) * 1000000 + end_parallel.tv_usec - start_parallel.tv_usec;
    printf("The result of the area using the parallel method is: %f\n", area);
    printf("Execution time for the parallel method is %f ns\n", total_parallel_time);

    //cleanup
    free(threads);
    free(thread_area);

    /* End Parallel portion */

}

void *integral(void *thread_area)
{
    double *p_thread_area = (double*)thread_area;
    double step = (b - a)/num_rect;
    double area = 0;
    double start = *p_thread_area / MAX_THREADS;
    double end = start + (b/MAX_THREADS);

    for(double i = start; i < end; i = (i+step))
    {
        area += step * sqrt(1-i*i);
    }
    *p_thread_area = area;
}

void print_usage(void)
{
    printf("usage: lab04 <number of intervals>\n");
}
