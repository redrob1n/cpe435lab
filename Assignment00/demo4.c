#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main()
{
    int pid;

    pid = fork();

    if(pid == 0)
    {
        printf("I am child, my ID is %i\n", getpid());
        printf("I am child, my parent is %i\n", getppid());
        printf("Child is sleeping for 10 seconds until when the parent might be dead already\n");
        sleep(10);
        printf("I am the same child with Id %i, but my parent id is %i\n", getpid(), getppid());
    }
    else
    {
        printf("I am the parent with id %i. My parent is %i and child is %i\n", getpid(), getppid());
        sleep(5);
    }

}
