#include <stdio.h>
#include <unistd.h>

int main()
{
    int x = 0;
    fork();
    x++;

    printf("I am process %i and my x is %i\n", getpid(), x);

    return 0;
}
