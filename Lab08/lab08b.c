#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <signal.h>
#include <unistd.h>
#include <time.h>

void alarmHandler(int sigval);

void kill_proc(int kilsignal);

void print_garbage(int sigval);

int main(int argc, char **argv)
{
    signal(SIGINT, print_garbage);
    signal(SIGALRM, alarmHandler);

    srand(time(NULL));

    for(;;);
}

void alarmHandler(int sigval)
{
    printf("Received signal %d\n", sigval);
    printf("Now you can kill me..\n");
    signal(SIGINT, kill_proc);
}

void kill_proc(int killsignal)
{
    exit(0);
}

void print_garbage(int sigval)
{
    for(int i = 0; i < 500; i++)
    {
        printf("%i",rand());
    }
    signal(SIGINT, print_garbage);
    alarm(10);
}


