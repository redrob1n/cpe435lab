#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <signal.h>
#include <unistd.h>

int pid = 0;

void alarmHandler(int sigval);

void kill_proc(int kilsignal);

void protected_message(int sigval);

void sigquit(int sigval);

int main(int argc, char **argv)
{
    signal(SIGINT, protected_message);
    signal(SIGALRM, alarmHandler);

    printf("Start program\n");

    pid = fork();
    if(pid == 0)
    {
        signal(SIGQUIT, sigquit);
    }
    else
    {
        alarm(10);
    }

    for(;;);
}

void alarmHandler(int sigval)
{
    printf("Received signal %d\n", sigval);
    printf("Now you can kill me..\n");
    signal(SIGINT, kill_proc);
}

void kill_proc(int killsignal)
{
    printf("Killing child\n");
    kill(pid, SIGQUIT);
    exit(0);
}

void protected_message(int sigval)
{
    printf("Ignoring kill signal: system is protected\n");
}

void sigquit(int sigval)
{
    printf("I have been killed my pid is: %i\n", pid);
    exit(0);
}
