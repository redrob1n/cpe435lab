/*
 * File: Lab05.h
 *
 * author: Chandler Ellis
 */

#ifndef _LAB05_H_
#define _LAB05_H_

struct info
{
    float value1, value2;
    float sum;
    int flag;
};
#define KEY ((key_t)(1234))
#define SEGSIZE sizeof (struct info)

#endif /* _LAB05_H_ */
