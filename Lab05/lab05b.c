/*
 * File: lab05b.c
 *
 * author: Chandler Ellis
 */

#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>

#include "lab05.h"

int main(int argc, char **argv)
{
    int id = shmget(KEY, SEGSIZE, 0);
    struct info *ctrl;

    if(id < 0)
    {
        printf("Error creating shared memory space\n");
        exit(1);
    }

    ctrl = shmat(id, 0, 0);
    if(ctrl <= (struct info*)(0))
    {
        printf("Error getting pointer to shared memort space\n");
        exit(1);
    }

    while(1)
    {
        ctrl->sum = 0;
        if(ctrl->flag == 1)
        {
            ctrl->sum = ctrl->value1 + ctrl->value2;
            printf("The sum is %f\n", ctrl->sum);
            ctrl->flag = 0;
        }

        if(ctrl->flag == -1)
        {
            break;
        }
    }



    return 0;
}
