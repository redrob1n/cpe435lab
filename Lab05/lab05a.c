/*
 * File: lab05a.c
 *
 * author: Chandler Ellis
 */

#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>

#include "lab05.h"

int main(int argc, char **argv)
{

    int id = 0;
    struct info *ctrl;

    id = shmget(KEY, SEGSIZE, IPC_CREAT | 0666);
    if(id < 0)
    {
        printf("Error creating shared memory space\n");
        exit(1);
    }

    ctrl = shmat(id, 0, 0);
    if(ctrl <= (struct info*)(0))
    {
        printf("Error getting pointer to shared memort space\n");
        exit(1);
    }

    ctrl->flag = 0;

    while(!(ctrl->flag))
    {
        for(int i = 0; i < 10; i++)
        {
            printf("Iteration: %i\n", i+1);
            printf("Enter the first value: ");
            scanf("%f", &(ctrl->value1));
            printf("Enter the second value: ");
            scanf("%f", &(ctrl->value2));
            ctrl->flag = 1;
        }
    }

    ctrl->flag = -1;
    shmdt(ctrl);
    shmctl(id, IPC_RMID, NULL);


    return 0;
}
